package Com.test.springbdd;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;

public class autorconsultar {
		   @Given("I am an author")
	    public void iAmAnAuthor() {
	        // Logic to check if the user is an author
	    }

	    @When("I access the evoraChair system")
	    public void accessEvoraChairSystem() {
	        // Logic to access the evoraChair system
	    }

	    @And("navigate to the article submission section")
	    public void navigateToArticleSubmissionSection() {
	        // Logic to navigate to the article submission section
	    }

	    @Then("I should be able to view the status of my submitted articles")
	    public void viewStatusOfSubmittedArticles() {
	        // Logic to verify that the status of the submitted articles is displayed
	    }
}
