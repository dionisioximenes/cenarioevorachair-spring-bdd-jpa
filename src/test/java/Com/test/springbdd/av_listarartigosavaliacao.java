package Com.test.springbdd;


import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.And;

public class av_listarartigosavaliacao {



	    @Given("I am an evaluator")
	    public void iAmAnEvaluator() {
	        // Logic to verify that the user is an evaluator
	    }

	    @When("I access the evoraChair system")
	    public void accessEvoraChairSystem() {
	        // Logic to simulate accessing the evoraChair system
	    }

	    @And("I navigate to the assigned articles section")
	    public void navigateToAssignedArticlesSection() {
	        // Logic to simulate navigating to the assigned articles section
	    }

	    @Then("I should be able to list and view all the articles assigned to me for evaluation")
	    public void listAndConsultAssignedArticles() {
	        // Logic to verify that the evaluator can list and view all the articles assigned for evaluation
	    }
}
