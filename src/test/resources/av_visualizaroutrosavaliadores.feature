Feature: av_visualizaroutrosavaliadores

  Scenario: View ratings from other reviewers
    Given I am an evaluator
    When I access the evoraChair system
    And I navigate to the article evaluations section
    Then I should be able to view the evaluations provided by other evaluators for each article
