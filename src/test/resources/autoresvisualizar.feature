Feature: autoresvisualizar

  Scenario: View ratings for all authors
    Given I am an author
    When I access the evoraChair system
    And the article evaluation process has been completed
    And the acceptance notifications have been sent
    Then I should be able to view the evaluations provided by all authors for each article
