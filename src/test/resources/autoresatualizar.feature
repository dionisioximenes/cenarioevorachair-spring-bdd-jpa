Feature: autoresatualizar

  Scenario: Update Articles
    Given I am an author
    And the article submission deadline has not passed
    When I access the evoraChair system
    And I navigate to the article submission section
    And I select the article I previously submitted
    And I update the article with new information or a revised PDF file
    Then the article is successfully updated