package com.test.springbdd;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class org_consultarartigosController {

    @GetMapping("/organizacao/artigos")
    public String org_consultarartigosController(@RequestParam("name") String name) {
        
        return "Consulta de artigos bem-sucedida: " + name;
    }
}
